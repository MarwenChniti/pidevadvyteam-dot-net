namespace Solution.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Solution.Domain.Entities;
    using Solution.Domain;
    using Solution.Data.Configurations;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<categorie> categories { get; set; }
        public virtual DbSet<centreinter> centreinters { get; set; }
        public virtual DbSet<commentaire> commentaires { get; set; }
        public virtual DbSet<competence> competences { get; set; }
        public virtual DbSet<evaluation> evaluations { get; set; }
        public virtual DbSet<formation> formations { get; set; }
        public virtual DbSet<mission> missions { get; set; }
        public virtual DbSet<participation> participations { get; set; }
        public virtual DbSet<projet> projets { get; set; }
        public virtual DbSet<publication> publications { get; set; }
        public virtual DbSet<questionreponsequiz> questionreponsequizs { get; set; }
        public virtual DbSet<questionreponseuser> questionreponseusers { get; set; }
        public virtual DbSet<quiz> quizs { get; set; }
        public virtual DbSet<quizuser> quizusers { get; set; }
        public virtual DbSet<rapport> rapports { get; set; }
        public virtual DbSet<timesheet> timesheets { get; set; }
        public virtual DbSet<user> users { get; set; }
        public virtual DbSet<user_formation> user_formation { get; set; }
        public virtual DbSet<user_mission> user_mission { get; set; }
        public virtual DbSet<user_rapport> user_rapport { get; set; }
        public virtual DbSet<question> questions { get; set; }
      


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new QuestionConfiguration());
            modelBuilder.Entity<categorie>()
                .Property(e => e.descreption)
                .IsUnicode(false);

            modelBuilder.Entity<categorie>()
                .Property(e => e.nom)
                .IsUnicode(false);

            modelBuilder.Entity<centreinter>()
                .Property(e => e.nom)
                .IsUnicode(false);

            modelBuilder.Entity<commentaire>()
                .Property(e => e.coment)
                .IsUnicode(false);

            modelBuilder.Entity<competence>()
                .Property(e => e.comptencePersonel)
                .IsUnicode(false);

            modelBuilder.Entity<competence>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<competence>()
                .Property(e => e.niveau)
                .IsUnicode(false);

            modelBuilder.Entity<evaluation>()
                .Property(e => e.endDate)
                .IsUnicode(false);

            modelBuilder.Entity<evaluation>()
                .Property(e => e.evaltype)
                .IsUnicode(false);

            modelBuilder.Entity<evaluation>()
                .Property(e => e.startDate)
                .IsUnicode(false);

            modelBuilder.Entity<formation>()
                .Property(e => e.date)
                .IsUnicode(false);

            modelBuilder.Entity<formation>()
                .Property(e => e.descreption)
                .IsUnicode(false);

            modelBuilder.Entity<formation>()
                .Property(e => e.lieu)
                .IsUnicode(false);

            modelBuilder.Entity<formation>()
                .Property(e => e.nom)
                .IsUnicode(false);

            modelBuilder.Entity<mission>()
                .Property(e => e.date)
                .IsUnicode(false);

            modelBuilder.Entity<mission>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<mission>()
                .Property(e => e.lieu)
                .IsUnicode(false);

            modelBuilder.Entity<mission>()
                .Property(e => e.objet)
                .IsUnicode(false);

            modelBuilder.Entity<participation>()
                .Property(e => e.nomFormation)
                .IsUnicode(false);

            modelBuilder.Entity<projet>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<projet>()
                .Property(e => e.nom)
                .IsUnicode(false);

            modelBuilder.Entity<publication>()
                .Property(e => e.image)
                .IsUnicode(false);

            modelBuilder.Entity<publication>()
                .Property(e => e.statut)
                .IsUnicode(false);

            modelBuilder.Entity<questionreponsequiz>()
                .Property(e => e.option1)
                .IsUnicode(false);

            modelBuilder.Entity<questionreponsequiz>()
                .Property(e => e.option2)
                .IsUnicode(false);

            modelBuilder.Entity<questionreponsequiz>()
                .Property(e => e.option3)
                .IsUnicode(false);

            modelBuilder.Entity<questionreponsequiz>()
                .Property(e => e.question)
                .IsUnicode(false);

            modelBuilder.Entity<questionreponsequiz>()
                .Property(e => e.reponse)
                .IsUnicode(false);

            modelBuilder.Entity<questionreponseuser>()
                .Property(e => e.question)
                .IsUnicode(false);

            modelBuilder.Entity<questionreponseuser>()
                .Property(e => e.reponse)
                .IsUnicode(false);

            modelBuilder.Entity<quiz>()
                .Property(e => e.competance)
                .IsUnicode(false);

            modelBuilder.Entity<quiz>()
                .Property(e => e.nom)
                .IsUnicode(false);

            modelBuilder.Entity<quizuser>()
                .Property(e => e.nom)
                .IsUnicode(false);

            modelBuilder.Entity<rapport>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<rapport>()
                .Property(e => e.objet)
                .IsUnicode(false);

            modelBuilder.Entity<timesheet>()
                .Property(e => e.nomTache)
                .IsUnicode(false);

            modelBuilder.Entity<timesheet>()
                .Property(e => e.descTache)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.login)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.nom)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.prenom)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.addresse)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.cin)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.image)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.imageCouverture)
                .IsUnicode(false);
        }
    }
}
