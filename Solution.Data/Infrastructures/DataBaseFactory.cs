﻿using Solution.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Data.Infrastructure
{
    public class DataBaseFactory : Disposable, IDataBaseFactory
    {
        private Model1 dataContext;

        public Model1 DataContext
        {
            get { return dataContext; }

        }

        public DataBaseFactory()
        {
            dataContext = new Model1();
        }
        protected override void DisposeCore() //libérer l'espace mémoire occupé par le ctx
        {
            if (DataContext != null)

                DataContext.Dispose();

        }


    }
}
