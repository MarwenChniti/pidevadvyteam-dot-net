﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Data.Infrastructure
{
    public interface IDataBaseFactory:IDisposable
    {
         Model1 DataContext { get;}
        //void Dispose(); hidden
    }
}
