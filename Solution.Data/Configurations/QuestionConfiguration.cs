﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Data.Configurations
{
   public class QuestionConfiguration : EntityTypeConfiguration<question>
    {
        public QuestionConfiguration()
        {
            /*ToTable("Question");
            HasKey(v => v.QstId);*/

            HasRequired(c => c.formation)
              .WithMany(cat1 => cat1.questions)
              .HasForeignKey(c => c.idFormation)
              .WillCascadeOnDelete(true);

        }
    }
}
