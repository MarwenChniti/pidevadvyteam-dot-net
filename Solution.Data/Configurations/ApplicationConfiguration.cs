﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Solution.Data.Configurations
{
   public class ApplicationConfiguration : EntityTypeConfiguration<application>
    {
        public ApplicationConfiguration()
        {
            ToTable("application");
            HasKey(v => v.AppId);

            HasRequired(c => c.formation)
           .WithMany(cat1 => cat1.applications)
           .HasForeignKey(c => c.idFormation)
           .WillCascadeOnDelete(true);


        }
    }
}
