﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
   public class application
    {

            [Key]
            public int AppId { get; set; }
            public String Nom { get; set; }
            public String Prenom { get; set; }
            public int OfferId { get; set; }
            public int CandId { get; set; }
            public String ApplicationStatus { get; set; }

           public int idFormation { get; set; }
           [ForeignKey("idFormation")]
           public virtual formation formation { get; set; }

    }
}
