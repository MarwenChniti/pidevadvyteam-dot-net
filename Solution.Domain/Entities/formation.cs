namespace Solution.Domain
{
    using Solution.Domain.Entities;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public enum Type { JEE, DOTNET, ANGULAR, FINANCE, JAVA }

    [Table("formation")]
    public partial class formation
    {
        public int id { get; set; }

        [StringLength(255)]
        public string date { get; set; }

        [StringLength(255)]
        public string descreption { get; set; }

        [Column(TypeName = "bit")]
        public bool etat { get; set; }

        [StringLength(255)]
        public string lieu { get; set; }

        public int nbrplace { get; set; }

        [StringLength(255)]
        public string nom { get; set; }

        public Type type { get; set; }

        public int? categorie_id { get; set; }

        public virtual ICollection<question> questions { get; set; }

        public virtual ICollection<application> applications { get; set; }
    }
}
