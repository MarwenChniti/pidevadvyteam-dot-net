namespace Solution.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("publication")]
    public partial class publication
    {
        public int id { get; set; }

        [StringLength(255)]
        public string image { get; set; }

        [StringLength(255)]
        public string statut { get; set; }

        public int? centreinter_id { get; set; }

        public int? user_id { get; set; }
    }
}
