namespace Solution.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("user")]
    public partial class user
    {
        public int id { get; set; }

        [StringLength(255)]
        public string login { get; set; }

        [StringLength(255)]
        public string nom { get; set; }

        [StringLength(255)]
        public string password { get; set; }

        [StringLength(255)]
        public string prenom { get; set; }

        public int? role { get; set; }

        [StringLength(255)]
        public string addresse { get; set; }

        [StringLength(255)]
        public string cin { get; set; }

        public DateTime? dat_naiss { get; set; }

        [StringLength(255)]
        public string email { get; set; }

        [StringLength(255)]
        public string image { get; set; }

        [StringLength(255)]
        public string imageCouverture { get; set; }

        public int? manager_id { get; set; }
    }
}
