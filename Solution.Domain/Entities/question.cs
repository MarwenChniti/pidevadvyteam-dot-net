﻿using Solution.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
   public class question
    {
        [Key]
        public int QstId { get; set; }
        public String qQst { get; set; }
        public String Rep1 { get; set; }
        public String Rep2 { get; set; }

        public String Rep3 { get; set; }

        public String Rep4 { get; set; }
        public String RepCorrect { get; set; }

        public int idFormation { get; set; }
        [ForeignKey("idFormation")]
        public virtual formation formation { get; set; }


    }
}
