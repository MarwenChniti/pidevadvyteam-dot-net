namespace Solution.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("quizuser")]
    public partial class quizuser
    {
        [Key]
        public int id_QuizUser { get; set; }

        [StringLength(255)]
        public string nom { get; set; }

        public int userID { get; set; }
    }
}
