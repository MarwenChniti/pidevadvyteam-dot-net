namespace Solution.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("questionreponsequiz")]
    public partial class questionreponsequiz
    {
        [Key]
        public int idQR { get; set; }

        [StringLength(255)]
        public string option1 { get; set; }

        [StringLength(255)]
        public string option2 { get; set; }

        [StringLength(255)]
        public string option3 { get; set; }

        [StringLength(255)]
        public string question { get; set; }

        [StringLength(255)]
        public string reponse { get; set; }

        public int? Id_quiz { get; set; }
    }
}
