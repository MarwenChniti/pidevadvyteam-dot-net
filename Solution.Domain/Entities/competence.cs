namespace Solution.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
   

    [Table("competences")]
    public partial class competence
    {
        [Key]
        public int id_competences { get; set; }

        [StringLength(255)]
        public string comptencePersonel { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        [StringLength(255)]
        public string niveau { get; set; }
    }
}
