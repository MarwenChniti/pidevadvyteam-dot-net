namespace Solution.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
  

    [Table("questionreponseuser")]
    public partial class questionreponseuser
    {
        [Key]
        public int idQR { get; set; }

        [StringLength(255)]
        public string question { get; set; }

        [StringLength(255)]
        public string reponse { get; set; }

        [Column(TypeName = "bit")]
        public bool status { get; set; }

        public int? Id_quiz_user { get; set; }
    }
}
