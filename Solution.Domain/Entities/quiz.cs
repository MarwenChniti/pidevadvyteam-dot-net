namespace Solution.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
   

    [Table("quiz")]
    public partial class quiz
    {
        [Key]
        public int id_Quiz { get; set; }

        [StringLength(255)]
        public string competance { get; set; }

        [StringLength(255)]
        public string nom { get; set; }
    }
}
