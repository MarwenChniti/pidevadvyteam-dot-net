namespace Solution.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
  

    [Table("commentaire")]
    public partial class commentaire
    {
        public int id { get; set; }

        [StringLength(255)]
        public string coment { get; set; }

        public int? publication_id { get; set; }

        public int? user_id { get; set; }
    }
}
