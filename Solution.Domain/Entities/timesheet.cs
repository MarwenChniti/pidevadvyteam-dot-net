namespace Solution.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("timesheet")]
    public partial class timesheet
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idProjet { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idUser { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(255)]
        public string nomTache { get; set; }

        [StringLength(255)]
        public string descTache { get; set; }

        public int? etat { get; set; }

        [Column(TypeName = "bit")]
        public bool isValid { get; set; }

        public int nbrHeures { get; set; }
    }
}
