namespace Solution.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("categorie")]
    public partial class categorie
    {
        public int id { get; set; }

        [StringLength(255)]
        public string descreption { get; set; }

        [StringLength(255)]
        public string nom { get; set; }
    }
}
