namespace Solution.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("mission")]
    public partial class mission
    {
        public int id { get; set; }

        [StringLength(255)]
        public string date { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        [StringLength(255)]
        public string lieu { get; set; }

        [StringLength(255)]
        public string objet { get; set; }
    }
}
