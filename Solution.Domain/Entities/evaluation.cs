namespace Solution.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
   

    [Table("evaluation")]
    public partial class evaluation
    {
        public int id { get; set; }

        [StringLength(255)]
        public string endDate { get; set; }

        [StringLength(255)]
        public string evaltype { get; set; }

        [StringLength(255)]
        public string startDate { get; set; }

        [Column(TypeName = "bit")]
        public bool? state { get; set; }

        public int? user_id { get; set; }
    }
}
