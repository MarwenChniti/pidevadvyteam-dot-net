﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Solution.Presentation.Areas.Administrateur.Controllers
{
    public class DashbordController : Controller
    {
        // GET: Administrateur/Dashbord
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Mission()
        {
            return View();
        }

        public ActionResult Evaluation()
        {
            return View();
        }

        public ActionResult Competences()
        {
            return View();
        }
    }
}