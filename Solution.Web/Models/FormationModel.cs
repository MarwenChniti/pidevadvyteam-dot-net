﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Solution.Web.Models
{
    public enum Type { JEE, DOTNET, ANGULAR, FINANCE, JAVA }
    public class FormationModel
    {
        public int id { get; set; }

       
        public string date { get; set; }

        
        public string descreption { get; set; }

        
        public bool etat { get; set; }

      
        public string lieu { get; set; }

        public int nbrplace { get; set; }

        
        public string nom { get; set; }

        public Type type { get; set; }
        public virtual ICollection<question> questions { get; set; }
    }
}