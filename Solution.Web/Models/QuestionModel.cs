﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Solution.Web.Models
{
    public class QuestionModel
    {
        public int QstId { get; set; }
        public String qQst { get; set; }
        public String Rep1 { get; set; }
        public String Rep2 { get; set; }

        public String Rep3 { get; set; }

        public String Rep4 { get; set; }
        public String RepCorrect { get; set; }

        public int idFormation { get; set; }
        public IEnumerable<SelectListItem> ls { get; set; }
    }
}