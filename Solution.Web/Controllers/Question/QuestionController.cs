﻿using Solution.Domain.Entities;
using Solution.Service.Services;
using Solution.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Solution.Web.Controllers.Question
{
    public class QuestionController : Controller
    {
        QuestionService iqs = new QuestionService();
        FormationService ts = new FormationService();
        // GET: Question
        public ActionResult Index()
        {
            List<QuestionModel> lq = new List<QuestionModel>();
            foreach (var qm in iqs.GetAll())
            {
                QuestionModel q = new QuestionModel();
                q.qQst = qm.qQst;
                q.QstId = qm.QstId;
                q.Rep1 = qm.Rep1;
                q.Rep2 = qm.Rep2;
                q.Rep3 = qm.Rep3;
                q.Rep4 = qm.Rep4;
                q.idFormation = qm.idFormation;
                q.RepCorrect = qm.RepCorrect;
                lq.Add(q);

            }
            return View(lq);
        }

        // GET: Question/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Question/Create
        public ActionResult Create()
        {
            var formations = ts.GetMany();
            ViewBag.TestId = new SelectList(formations, "id", "nom");
            
            /*var offres = OS.GetMany();
            ViewBag.Offre_Id = new SelectList(offres, "Offre_Id", "Offre_Name");*/

            return View();
        }

        // POST: Question/Create
        [HttpPost]
        public ActionResult Create(QuestionModel qm)
        {
             question q = new question();
             q.qQst = qm.qQst;
             q.Rep1 = qm.Rep1;
             q.Rep2 = qm.Rep2;
             q.Rep3 = qm.Rep3;
             q.Rep4 = qm.Rep4;
             q.idFormation = qm.idFormation;
             q.RepCorrect = qm.RepCorrect;
             iqs.Add(q);
             iqs.Commit();
            return RedirectToAction("ShowQuestions");

        }

        public ActionResult ShowQuestions()
        {
            return View(iqs.GetMany());
        }

        [HttpPost]
        public ActionResult ShowQuestions(int id)
        {
            var list = iqs.GetQuestiontByFormation(id);


            return View(list);



        }

        // GET: Question/Edit/5
        public ActionResult Edit(int id)
        {

            question t = iqs.GetById(id);
            if (t == null)
            {
                return HttpNotFound();
            }
            return View(t);
        }

        // POST: Test/Edit/5
        [HttpPost]
        public ActionResult Edit(question p)
        {
            question p1 = iqs.GetById(p.QstId);
            p1.qQst = p.qQst;
            p1.Rep1 = p.Rep1;
            p1.Rep2 = p.Rep2;
            p1.Rep3 = p.Rep3;
            p1.Rep4 = p.Rep4;
            p1.RepCorrect = p.RepCorrect;

            if (ModelState.IsValid)
            {
                iqs.Update(p1);
                iqs.Commit();
                return RedirectToAction("ShowQuestions");
            }
            return RedirectToAction("ShowQuestions");
        }

        // GET: Question/Delete/5
        public ActionResult Delete(int id)
        {

            return View(iqs.GetById(id));
        }

        // POST: Question/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, question q)
        {
            q = iqs.GetById(id);
            iqs.Delete(q);
            iqs.Commit();
            return RedirectToAction("ShowQuestions");

        }

    }
}
