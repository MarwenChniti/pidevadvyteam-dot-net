﻿using Solution.Domain;
using Solution.Domain.Entities;
using Solution.Service.Services;
using Solution.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Solution.Web.Controllers.Formation
{
    public class TestController : Controller
    {
        FormationService cs = new FormationService();
        QuestionService iqs = new QuestionService();
        public ActionResult Index()
        {

            /* int user = Int16.Parse(User.Identity.GetUserId());
             var q = itm.GetAll();*/
            {
                FormationModel tm = new FormationModel();
                formation c = new formation();
                List<FormationModel> l = new List<FormationModel>();
                List<question> lq = new List<question>();
                c = cs.GetAll().OrderByDescending(t => t.questions.Count).First();
                c.id = tm.id;
                tm.nom = c.nom;
              
                lq = c.questions.OrderBy(x => Guid.NewGuid()).Take(20).ToList();
                tm.questions = lq;
                l.Add(tm);

                return View(l);
            }

        }

        // GET: Test/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Test/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Test/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Test/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Test/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Test/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Test/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult TestResult()
        {
            {
                FormationModel tm = new FormationModel();
                formation c = new formation();
                List<FormationModel> l = new List<FormationModel>();
                List<question> lq = new List<question>();
                c = cs.GetAll().OrderByDescending(t => t.questions.Count).First();
                tm.nom = c.nom;
                
                lq = c.questions.OrderBy(x => Guid.NewGuid()).Take(20).ToList();
                tm.questions = lq;
                l.Add(tm);

                return View(l);
            }
        }
        public ActionResult TestLang()
        {
            {
                FormationModel tm = new FormationModel();
                formation c = new formation();
                List<FormationModel> l = new List<FormationModel>();
                List<question> lq = new List<question>();
                c = cs.GetAll().OrderByDescending(t => t.questions.Count).First();
                tm.nom = c.nom;

                lq = c.questions.OrderBy(x => Guid.NewGuid()).Take(20).ToList();
                tm.questions = lq;
                l.Add(tm);

                return View(l);
            }
        }
        public ActionResult TestPsy()
        {
            return View();
        }
    }
}
