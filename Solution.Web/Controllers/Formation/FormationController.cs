﻿
using RecrutementService.Services;
using Solution.Domain;
using Solution.Domain.Entities;
using Solution.Service.Services;
using Solution.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Net.Mail;
using System.Net;

namespace Solution.Web.Controllers.Formation
{
    public class FormationController : Controller
    {
        ApplicationService aps = new ApplicationService();
        FormationService ts = new FormationService();
        // GET: Formation
        public ActionResult Index()
        {

            System.Net.Http.HttpClient Client = new System.Net.Http.HttpClient();
            Client.BaseAddress = new Uri("http://localhost:9080");
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            System.Net.Http.HttpResponseMessage response = Client.GetAsync("pidevADVYTEAM-web/formation").Result;
            if (response.IsSuccessStatusCode)
            {
                ViewBag.result = response.Content.ReadAsAsync<IEnumerable<formation>>().Result;
            }
            else
            {
                ViewBag.result = "error";
            }
            return View();
        }

        // GET: Formation/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Formation/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Formation/Create
        [HttpPost]
        public ActionResult Create(FormationModel f)
        {
            HttpClient client = new HttpClient();

            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, "http://localhost:9080/pidevADVYTEAM-web/formation/new");
            string json = new JavaScriptSerializer().Serialize(new
            {
                nom = f.nom,
                descreption = f.descreption,
                date = f.date,
                nbrplace = f.nbrplace,
                lieu = f.lieu,



            }
                );
            requestMessage.Content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.SendAsync(requestMessage).GetAwaiter().GetResult();
            return RedirectToAction("Index");
        }

        // GET: Formation/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Formation/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Formation/Delete/5

        public ActionResult Delete(int id)
        {
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("http://localhost:9080");
            Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage responce = Client.DeleteAsync("/pidevADVYTEAM-web/formation/" + id).Result;
            return RedirectToAction("Index");
        }

        // POST: Formation/Delete/5
        /* [HttpPost]
         public ActionResult Delete(int id, FormCollection collection)
         {
             try
             {
                 // TODO: Add delete logic here

                 return RedirectToAction("Index");
             }
             catch
             {
                 return View();
             }
         }*/


        //////////////////////////////////////////////// application ////////////////////////////////
        ///
        public ActionResult Apply()
        {
            var formations = ts.GetMany();
            ViewBag.TestId = new SelectList(formations, "id", "nom");

            /*var offres = OS.GetMany();
            ViewBag.Offre_Id = new SelectList(offres, "Offre_Id", "Offre_Name");*/

            return View();
        }

        // POST: Question/Create
        [HttpPost]
        public ActionResult Apply(ApplicationModel qm)
        {
            application q = new application();

            q.Nom = qm.Nom;
            q.Prenom = qm.Prenom;

            q.idFormation = qm.idFormation;

            aps.Add(q);
            aps.Commit();
            return RedirectToAction("Index");

        }
        public ActionResult Showapp()
        {
            return View(aps.GetMany());
        }

        [HttpPost]
        public ActionResult Index(string filtre)
        {
            var list = aps.GetMany();


            // recherche
            /* if (!String.IsNullOrEmpty(filtre))
             {
                 list = list.Where(m => m.Name.ToString().Equals(filtre)).ToList();
             }*/

            return View(list);



        }
        [HttpPost]
        public ActionResult Confirmer(application p)
        {
            application p1 = aps.GetById(p.AppId);
            p1.ApplicationStatus = p.ApplicationStatus;


            if (ModelState.IsValid)
            {
                aps.Update(p1);
                aps.Commit();
                var verifyurl = "/Signup/VerifiyAccount/";
                var link = Request.Url.AbsolutePath.Replace(Request.Url.PathAndQuery, verifyurl);

                var fromEmail = new MailAddress("fatma.dayeg@esprit.tn", "AdvyTeam");
                var toEmail = new MailAddress("chniti.marwen@gmail.com");
                var FromEmailPassword = "172jft1681";

                string subject = "Response On Formation Application";

                string body = "We have respond on your Formation application! Go check your Result. " +
                    "<br/><a href = '" + link + "'>" + link + "</a>";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromEmail.Address, FromEmailPassword),
                    Timeout = 20000
                };
                using (var message = new MailMessage(fromEmail, toEmail)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true

                }) smtp.Send(message);
                return RedirectToAction("Showapp");
            }
            return RedirectToAction("Showapp");
        }
        public ActionResult Confirmer(int id)
        {

            application t = aps.GetById(id);
            if (t == null)
            {
                return HttpNotFound();
            }
            return View(t);
        }

        public ActionResult Supprimer(int id, application p)
        {

            p = aps.GetById(id);
            aps.Delete(p);
            aps.Commit();
            return RedirectToAction("Showapp");
        }
        public ActionResult ShowAppCand()
        {
            return View(aps.GetMany());
        }

        [HttpPost]
        public ActionResult ShowAppCand(string filtre)
        {
            var list = aps.GetMany();


            // recherche
            /* if (!String.IsNullOrEmpty(filtre))
             {
                 list = list.Where(m => m.Name.ToString().Equals(filtre)).ToList();
             }*/

            return View(list);

        }
    }
}
