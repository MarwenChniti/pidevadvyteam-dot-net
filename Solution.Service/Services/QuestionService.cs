﻿using Service.Pattern;
using Solution.Data.Infrastructure;
using Solution.Domain.Entities;
using Solution.Service.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Service.Services
{
    public class QuestionService : Service<question>, IQuestionService
    {
        static IDataBaseFactory dbfactor = new DataBaseFactory();
        static IUnitOfWork wow = new UnitOfWork(dbfactor);
        // IDataBaseFactory dbfactory = null;
        public QuestionService() : base(wow)
        {

        }

        public IEnumerable<question> GetQuestiontByFormation(int FormationId)
        {
            return GetMany(p => p.formation.id == FormationId);
        }

        
    }
}
